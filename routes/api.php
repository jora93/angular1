<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('authenticate', 'AuthenticateController@index');
Route::post('authenticate', 'AuthenticateController@authenticate');

Route::group(['middleware' => 'jwt.auth'], function()   {
    Route::post('stripe-checkout', 'StripeController@checkout');
    Route::resource('categories', 'CategoryController');
    Route::resource('products', 'ProductController');
    Route::post('search/products', 'ProductController@search');
    route::get('user/auth', 'UserController@getAuth');
    Route::resource('purchase', 'PurchaseController');
});
