<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
class ProductController extends Controller
{

    public function __construct(Product $products)
    {
        $this->middleware('jwt.auth');
        $this->products = $products;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if ($products = $this->products->all()->random(6)) {
//            if ($products = $this->products->get()) {
            return response()->json(['products' => $products]);
        }
        return response()->json(['message' => 'Something Went Wrong'], 400);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $inputs = $request->all();
        if (isset($inputs['file'])) {
            $file = $inputs['file'];
            $filename = Carbon::now()->timestamp.$inputs['file_name'];
            Storage::disk('angular_products_images')->put($filename, file_get_contents($file));
            $inputs['image_src'] = '/assets/images/products/'.$filename;
        }
        if ($product = $this->products->create($inputs)) {
            return response()->json(['product' => $product]);
        }
        return response()->json(['message' => 'Something Went Wrong'], 400);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if ($product = $this->products->with('category')->find($id)) {
            return response()->json(['product' => $product]);
        }
        return response()->json(['message' => 'Something Went Wrong'], 400);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if ($product = $this->products->find($id)) {
            return response()->json(['product' => $product]);
        }
        return response()->json(['message' => 'Something Went Wrong'], 400);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $inputs     = $request->all();
        $product    = $this->products->find($id);

        if (isset($inputs['file'])) {
            $file = $inputs['file'];
            $filename = Carbon::now()->timestamp.$inputs['file_name'];
            Storage::disk('angular_products_images')->put($filename, file_get_contents($file));
            $inputs['image_src'] = '/assets/images/products/'.$filename;

            Storage::disk('angular_app')->delete( $product->image_src );
        }
        if ($product->update($inputs)) {
            return response()->json(['message' => 'Product updated successfully']);
        }
        return response()->json(['message' => 'Something Went Wrong'], 400);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = $this->products->find($id);

        if ($product ->delete()) {
            Storage::disk('angular_app')->delete( $product->image_src );
            return response()->json(['message' => 'Product deleted successfully']);
        }
        return response()->json(['message' => 'Something Went Wrong'], 400);
    }

    public function search(Request $request)
    {
        $inputs = $request->all();

        $products = $this->products->when(isset($inputs['category']), function ($query) use ($inputs) {
            return $query->where('category_id', $inputs['category']);
        })->when(isset($inputs['name']), function ($query) use ($inputs) {
            return $query->where('name', 'like', "%{$inputs['name']}%" );
        })->when(isset($inputs['currency']), function ($query) use ($inputs) {
            return $query->where('currency', $inputs['currency']);
        })->when(isset($inputs['price_from']), function ($query) use ($inputs) {
            return $query->where('price', '>=', $inputs['price_from']);
        })->when(isset($inputs['price_to']), function ($query) use ($inputs) {
            return $query->where('price', '<=', $inputs['price_to']);
        })->get();

        return response()->json(['products' => $products]);

    }
}
