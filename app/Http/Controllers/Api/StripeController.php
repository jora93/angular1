<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Stripe\Stripe;
use Stripe\Charge;
use Stripe\Customer;
use App\User;
use Auth;
class StripeController extends Controller
{

    public function __construct()
    {
        $this->middleware('jwt.auth');
        Stripe::setApiKey(env('STRIPE_SECRET_KEY'));
    }

    public function checkout(Request $request, User $user)
    {
        if(null != Auth::user()->customer_id) {
            $customer_id = Auth::user()->customer_id;
        } else {
            try {
                $customer = Customer::create(array(
                    'email' => $request->email,
                    'source'  => $request->id
                ));
                $user = $user->find(Auth::user()->id);
                $user->customer_id = $customer->id;
                $user->save();
                $customer_id = $customer->id;
            } catch(\Exception $e) {
                return $e->getMessage();
            }
        }
        try {
            $charge = Charge::create(array(
                'customer' => $customer_id,
                'amount'   => $request->amount,
                'currency' => $request->currency
            ));
        } catch (\Exception $e) {
            return $e->getMessage();
        }

        return response()->json(['charge' => $charge], 200);
    }
}
