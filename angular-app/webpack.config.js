"use strict";
let webpack = require("webpack");

module.exports = {
    context: __dirname + '/app',
    entry: [
        './app',
        './run',
        './angular-stripe-checkout.js',
        './Controllers/auth/AuthController',
        './Controllers/auth/LogoutController',
        './Controllers/admin/AdminController',
        './Controllers/admin/AdminCategoryController',
        './Controllers/admin/AdminProductController',
        './Controllers/HomeController',
        './Controllers/ProductController',
        './Controllers/PurchaseController',
        './Directives/header',
        './Directives/sidebar',
        './Directives/fileread',
        './Services/CategoryService',
        './Services/ProductService',
        './Services/StripeService',
        './Services/UserService',
        './Services/PurchaseService',
        './Middlewares/AuthMiddleware',
        './Middlewares/AdminMiddleware'
    ],
    output: {
        path: __dirname + '/assets/js',
        filename: 'app.bind.js',
    },
    watch: true,
    watchOptions: {
        aggregateTimeout: 100
    },
    devtool: 'inline-source-map',

    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env']
                    }
                }
            }
        ]
    },
    plugins: [
        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery"
        })
    ]
};