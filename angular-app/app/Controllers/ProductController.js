angular
    .module('App')
    .controller('ProductViewCtrl', 
    ['ProductService', 'StripeService', 'UserService', 'PurchaseService', '$scope', '$stateParams', 
    function (ProductService, StripeService, UserService, PurchaseService, $scope, $stateParams, ) {
 
        ProductService.getById($stateParams.id).then(function(data) {
            $scope.product = data;
        }, function(reason) {
            $scope.error = reason;
        });

        UserService.getAuth().then(function(data) {
            $scope.user = data;
        }, function(reason) {
            $scope.error = reason;
        });

        $scope.doCheckout = function (token) {
            token.amount = $scope.product.price + '00';
            token.currency = $scope.product.currency.toLowerCase();
            console.log(token);

            StripeService.checkout(token).then(function (data) {
                console.log(data);

                let purchaseData = {};
                purchaseData.product_id = $scope.product.id;
                purchaseData.price      = $scope.product.price;
                purchaseData.currency   = $scope.product.currency;

                PurchaseService.create(purchaseData).then(function(data) {
                    $scope.success = data;
                }, function (reason) {
                    $scope.error = reason;
                });
            }, function (reason) {
                $scope.error = reason;
            });
        };

    }]);