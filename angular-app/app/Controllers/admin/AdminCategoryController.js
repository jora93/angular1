angular
    .module('App')
    .controller('AdminCategoryController', AdminCategoryController);

function AdminCategoryController($state, $stateParams, CategoryService) {
    let vm = this;
    vm.categories;
    vm.error;
    vm.success;
    vm.name;
    vm.headerItems = JSON.parse(localStorage.getItem('headerItems'));

    getCategories = function(){
        CategoryService.getAll().then(function(data) {
            vm.categories = data;
        }, function(reason) {
            console.log(reason);
        });
    };
    getCategories();

    vm.store = function () {
        if ( vm.name ) {
            CategoryService.store(vm.name).then(function(data) {
                vm.success = 'Category created successfully';
                $state.go('adminCategories', {});
            }, function(reason) {
                vm.error = reason;
            });
        }
    }

    if ($stateParams.id) {
        CategoryService.getById($stateParams.id).then(function(data) {
          vm.category = data;
        }, function(reason) {
            vm.error = reason;
        })
    }

    vm.update = function () {
        console.log('update');
        CategoryService.update(vm.category.id, vm.category.name).then(function(data) {
            $state.go('adminCategories', {});
        }, function(reason) {
            vm.error = reason;
        });

    };


    vm.delete = function (id) {
        CategoryService.delete(id).then(function(data) {
            console.log(data);
            getCategories();
            $state.go('adminCategories', {});
        }, function(reason) {
            vm.error = reason;
        })

    }
}

