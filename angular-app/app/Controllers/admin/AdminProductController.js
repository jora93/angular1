angular
    .module('App')
    .controller('AdminProductIndexCtrl', ['ProductService', 'CategoryService', '$scope', function( ProductService, CategoryService, $scope ) {

        $scope.searchData = {};

        ProductService.getRandom().then(function(data) {
            $scope.products = data;
        }, function(reason) {
            $scope.error = reason;
        });

        CategoryService.getAll().then(function(data) {
            $scope.categories = data;
        }, function(reason) {
            $scope.error = reason;
        });

        $scope.search = function () {
            ProductService.search($scope.searchData).then(function(data) {
                $scope.products = data;
            }, function(reason) {
                $scope.error = reason;
            });
        }
    }])
    .controller('AdminProductViewCtrl', [ 'ProductService','$stateParams', '$scope', '$state', function( ProductService, $stateParams, $scope, $state) {
        ProductService.getById($stateParams.id).then(function(data) {
            $scope.product = data;
        }, function(reason) {
            $scope.error = reason;
        });

        $scope.delete = function (id) {
            ProductService.delete(id).then(function(data) {
                $state.go('adminProductsIndex');
            }, function(reason) {
                $scope.error = reason;
            });
        }
    }])
    .controller('AdminProductCreateCtrl', [ 'ProductService', 'CategoryService', '$scope', '$state', function( ProductService, CategoryService, $scope, $state) {
        $scope.data =  null;
        CategoryService.getAll().then(function(data) {
            $scope.categories = data;
        }, function(reason) {
            $scope.error = reason;
        });
        $scope.fileNameChanged = function (ele) {
            let file = ele.files[0];
            if ( file.type.substring(0, 5) != 'image' ) {
                $scope.error = "file type must be Image !!!"
            } else {
                $scope.data.file_name = file.name;
            }
        }

        $scope.store = function() {
            ProductService.store($scope.data).then(function(data) {
                $state.go('adminProductsView', {id:data.id});
            }, function(reason) {
                $scope.error = reason;
            });
        }

    }])
    .controller('AdminProductEditCtrl', [ 'ProductService', 'CategoryService', '$stateParams', '$scope', '$state', function( ProductService, CategoryService, $stateParams, $scope, $state) {

        $scope.currencies = [
            {name: 'USD'},
            {name: 'EUR'}
        ];

        ProductService.getById($stateParams.id).then(function(data) {
            $scope.product = data;
            $scope.product.originalImageSrc = data.image_src;

        }, function(reason) {
            $scope.error = reason;
        });


        CategoryService.getAll().then(function(data) {
            $scope.categories = data;
        }, function(reason) {
            $scope.error = reason;
        });

        $scope.fileNameChanged = function (ele) {
            if (ele.files && ele.files[0] && (ele.files[0].type.substring(0, 5) == 'image')) {
                let reader = new FileReader();

                reader.onload = function(e) {
                    $scope.product.image_src = e.target.result;
                }

                reader.readAsDataURL(ele.files[0]);
                $scope.product.file_name = ele.files[0].name;
                $scope.isImageChanged = true;
            }
        }

        $scope.deleteImage = function() {
            $('#editProductImageInput').val('')
            $scope.product.image_src = $scope.product.originalImageSrc;
            $scope.isImageChanged = false;
        }

        $scope.store = function() {

            let data = {
                category_id : $scope.product.category_id,
                name        : $scope.product.name,
                price       : $scope.product.price,
                currency    : $scope.product.currency,

            };

            if ($scope.isImageChanged) {
                data.file       = $scope.product.image_src;
                data.file_name  = $scope.product.file_name;
            };

            ProductService.update($scope.product.id, data).then(function(data) {
                $state.go('adminProductsView', {id: $scope.product.id});
            }, function(reason) {
                $scope.error = reason;
            });
        }

    }]);