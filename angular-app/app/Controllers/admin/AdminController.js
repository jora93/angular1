angular
    .module('App')
    .controller('AdminController', AdminController);

function AdminController($http) {
    let vm = this;
    vm.users;
    vm.error;

    $http.get('http://angular1.loc/api/authenticate')
        .then(function (response) {
            vm.users = response.data;
        },
        function (response) {
            vm.error = response.message;
        });
}