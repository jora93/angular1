angular
    .module('App')
    .controller('HomeCtrl', ['ProductService', 'CategoryService', '$scope', function (ProductService, CategoryService, $scope) {

        $scope.searchData = {};

        ProductService.getRandom().then(function(data) {
            $scope.products = data;
        }, function(reason) {
            $scope.error = reason;
        });

        CategoryService.getAll().then(function(data) {
            $scope.categories = data;
        }, function(reason) {
            $scope.error = reason;
        });

        $scope.search = function () {
            ProductService.search($scope.searchData).then(function(data) {
                $scope.products = data;
            }, function(reason) {
                $scope.error = reason;
            });
        }
}]);