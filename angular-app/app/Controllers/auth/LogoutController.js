angular
    .module('App')
    .controller('LogoutController', LogoutController);

function LogoutController($auth, $state) {

    (function(){
        $auth.logout().then(function() {
            localStorage.clear();
            $state.go('auth');
        })
    })();


}