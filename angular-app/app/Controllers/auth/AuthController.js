angular
    .module('App')
    .controller('AuthController', AuthController);

function AuthController($auth, $state) {

    let vm = this;
    vm.headerItems = [
        { "sref": "adminHome", "text": "Admin Home" },
        { "sref": "adminCategories", "text": "Categories" },
        { "sref": "adminProductsIndex", "text": "Products" }
    ];

    vm.login = function () {
        localStorage.clear();

        let credentials = {
            email: vm.email,
            password: vm.password
        };

        $auth.login(credentials).then(function (response) {

            localStorage.setItem('headerItems', JSON.stringify(vm.headerItems));

            if (response.data.user.is_admin) {
                localStorage.setItem('isAdmin', true);
                $state.go('adminHome');
            } else {
                localStorage.setItem('isAdmin', false);
                $state.go('home');
            }
        });
    }

}