angular
    .module('App')
    .service('UserService', function ($http, $q) {

        this.getAuth = function () {
            let user = $http.get('http://angular1.loc/api/user/auth')
                .then(function (response) {
                        return response.data.user;
                    },
                    function (response) {
                    });

            return $q((resolve, reject) => {
                if (user) {
                    resolve(user);
                } else {
                    reject('Something went wrong');
                }
            });
        };
    });