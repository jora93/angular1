angular
    .module('App')
    .service('CategoryService', function($http, $q) {

        this.getAll = function () {
            let categories = $http.get('http://angular1.loc/api/categories')
                .then(function (response) {
                        return response.data.categories;
                    },
                    function (response) {
                    });

            return $q((resolve, reject) => {
                if (categories) {
                    resolve(categories);
                } else {
                    reject('Something went wrong');
                }
            });
        };

        this.store = function(name) {
            let category = $http.post(
                'http://angular1.loc/api/categories',
                { name: name }
            ).then(function (response) {
                    return response.data.category;
                },
                function (response) {
                });

            return $q((resolve, reject) => {
                if (category) {
                    resolve(category);
                } else {
                    reject('Something went wrong');
                }
            });
        }

        this.update = function(id, name) {
            let categoryUpdate = $http.put(
                'http://angular1.loc/api/categories/' + id,
                {
                    name: name
                }
            ).then(function (response) {
                    return response.data.message;
                },
                function (response) {
                });

            return $q((resolve, reject) => {
                if (categoryUpdate) {
                    resolve(categoryUpdate);
                } else {
                    reject('Something went wrong');
                }
            });
        }

        this.getById = function(id) {
            let category = $http.get(
                'http://angular1.loc/api/categories/' + id + '/edit',
            ).then(function (response) {
                    return response.data.category;
                },
                function (response) {
                    return response.data.message;
                });

            return $q((resolve, reject) => {
                if (category) {
                    resolve(category);
                } else {
                    reject(response.data.message);
                }
            });
        }

        this.delete = function(id) {
            let destroy = $http.delete(
                'http://angular1.loc/api/categories/' + id ,
            ).then(function (response) {
                    return response.data.message;
                },
                function (response) {
                    return response.data.message;
                });

            return $q((resolve, reject) => {
                if (destroy) {
                    resolve(destroy);
                } else {
                    reject(response.data.message);
                }
            });
        }
    });