angular
    .module('App')
    .service('ProductService', function($http, $q) {

        this.getRandom = function () {
            let products = $http.get('http://angular1.loc/api/products')
                .then(function (response) {
                    return response.data.products;
                    },
                    function (response) {
                    });

            return $q((resolve, reject) => {
                if (products) {
                    resolve(products);
                } else {
                    reject('Something went wrong');
                }
            });
        };

        this.getById = function (id) {
            let product = $http.get( 'http://angular1.loc/api/products/' + id )
                .then(function (response) {
                        return response.data.product;
                    },
                    function (response) {
                    });

            return $q((resolve, reject) => {
                if (product) {
                    resolve(product);
                } else {
                    reject('Something went wrong');
                }
            });
        }

        this.store = function (data) {
            let product = $http.post( 'http://angular1.loc/api/products', data)
                .then(function (response) {
                        return response.data.product;
                    },
                    function (response) {
                    });

            return $q((resolve, reject) => {
                if (product) {
                    resolve(product);
                } else {
                    reject('Something went wrong');
                }
            });
        }

        this.update = function (id, data) {
            let product = $http.put( 'http://angular1.loc/api/products/' + id, data)
                .then(function (response) {
                        return response.data.message;
                    },
                    function (response) {
                    });

            return $q((resolve, reject) => {
                if (product) {
                    resolve(product);
                } else {
                    reject('Something went wrong');
                }
            });
        }

        this.delete = function (id) {
            let product = $http.delete( 'http://angular1.loc/api/products/' + id)
                .then(function (response) {
                        return response.data.message;
                    },
                    function (response) {
                    });

            return $q((resolve, reject) => {
                if (product) {
                    resolve(product);
                } else {
                    reject('Something went wrong');
                }
            });
        }

        this.search = function (data) {
            let products = $http.post( 'http://angular1.loc/api/search/products', data)
                .then(function (response) {
                        return response.data.products;
                    },
                    function (response) {
                    });

            return $q((resolve, reject) => {
                if (products) {
                    resolve(products);
                } else {
                    reject('Something went wrong');
                }
            });
        }
    });