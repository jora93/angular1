angular
    .module('App')
    .service('StripeService', function ($http, $q) {

        this.checkout = function (data) {
            let stripeData = $http.post('http://angular1.loc/api/stripe-checkout', data)
                .then(function (response) {
                    return response.data;
                },
                    function (response) {
                    });

            return $q((resolve, reject) => {
                if (stripeData) {
                    resolve(stripeData);
                } else {
                    reject('Something went wrong');
                }
            });
        };
    });