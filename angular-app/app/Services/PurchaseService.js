angular
    .module('App')
    .service('PurchaseService', function ($http, $q) {

        this.create = function (data) {
            let purchase = $http.post('http://angular1.loc/api/purchase', data)
                .then(function (response) {
                    return response.data.message;
                },
                    function (response) {
                    });

            return $q((resolve, reject) => {
                if (purchase) {
                    resolve(purchase);
                } else {
                    reject('Something went wrong');
                }
            });
        };
    });