angular
    .module('App')
    .directive('appHeader', function() {
        return {
            controller: function($scope) {
                $scope.headerItems = JSON.parse(localStorage.getItem('headerItems'))
            },
            templateUrl: '../views/parts/header.html'
        };

    });

