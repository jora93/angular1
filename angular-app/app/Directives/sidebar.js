angular
    .module('App')
    .directive('appSidebar', function() {
        return {
            controller: function($scope) {
                $scope.sidebarItems = JSON.parse(localStorage.getItem('headerItems'))
            },
            templateUrl: '../views/parts/sidebar.html'
        };

    });

