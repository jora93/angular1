import angular from 'angular';
import 'angular-ui-router';
import 'satellizer'

let App = angular.module('App', ['ui.router', 'satellizer', "stripe.checkout"]);

App.config(function ($stateProvider, $urlRouterProvider, $authProvider, $locationProvider) {

    $authProvider.loginUrl = 'http://angular1.loc/api/authenticate';

    $urlRouterProvider.otherwise('/auth');
    
    $stateProvider

    /*
    * auth routes
    * */
    .state('auth', {
        url: '/auth',
        templateUrl: '../views/auth/authView.html',
        controller: 'AuthController as auth'
    })
    .state('logout', {
        url: '/logout',
        controller: 'LogoutController'
    })

    /*
    * admin routes
    * */
    .state('adminHome', {
        url: '/admin/home',
        templateUrl: '../views/admin/home.html',
        controller: 'AdminController as admin',
        middleware: ['authMiddleware', 'adminMiddleware']
    }).state('adminCategories', {
        url: '/admin/categories',
        templateUrl: '../views/admin/category/index.html',
        controller: 'AdminCategoryController as adminCategory',
        middleware: ['authMiddleware', 'adminMiddleware']
    }).state('adminCategoriesCreate', {
        url: '/admin/categories/create',
        templateUrl: '../views/admin/category/create.html',
        controller: 'AdminCategoryController as adminCategory',
        middleware: ['authMiddleware', 'adminMiddleware']
    }).state('adminCategoriesEdit', {
        url: '/admin/categories/edit/{id}',
        templateUrl: '../views/admin/category/edit.html',
        controller: 'AdminCategoryController as adminCategory',
        middleware: ['authMiddleware', 'adminMiddleware']
    }).state('adminProductsIndex', {
        url: '/admin/products',
        templateUrl: '../views/admin/product/index.html',
        controller: 'AdminProductIndexCtrl as adminProductIndex',
        middleware: ['authMiddleware', 'adminMiddleware']
    }).state('adminProductsView', {
        url: '/admin/products/{id}',
        templateUrl: '../views/admin/product/view.html',
        controller: 'AdminProductViewCtrl as adminProductView',
        middleware: ['authMiddleware', 'adminMiddleware']
    }).state('adminProductsCreate', {
        url: '/admin/products/create',
        templateUrl: '../views/admin/product/create.html',
        controller: 'AdminProductCreateCtrl as adminProductCreate',
        middleware: ['authMiddleware', 'adminMiddleware']
    }).state('adminProductsEdit', {
        url: '/admin/products/edit/{id}',
        templateUrl: '../views/admin/product/edit.html',
        controller: 'AdminProductEditCtrl as adminProductEdit',
        middleware: ['authMiddleware', 'adminMiddleware']
    })

    /*
    * user routes
    * */
    .state('home', {
        url: '/home',
        templateUrl: '../views/home.html',
        controller: 'HomeCtrl as home',
        middleware: ['authMiddleware']
    }).state('productView', {
        url: '/product/{id}',
        templateUrl: '../views/product/index.html',
        controller: 'ProductViewCtrl',
        middleware: ['authMiddleware']
    }).state('purchaseIndex', {
        url: '/purchase/{userId}',
        templateUrl: '../views/purchase/index.html',
        controller: 'PurchaseIndexCtrl',
        middleware: ['authMiddleware']
    });

    $locationProvider.html5Mode(true);
        
});

