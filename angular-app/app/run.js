angular
    .module('App')
    .run(function ($state, $rootScope, $injector, $transitions) {

        $transitions.onStart({}, function ( transition ) {

            let currentState = transition.to()

            callMiddlewares(currentState);

            function callMiddlewares(state){
                if(state && state.hasOwnProperty('middleware')){
                    if (typeof currentState.middleware === 'object') {
                        angular.forEach(state.middleware, function (middleWare) {
                            callMiddleware(middleWare);
                        });
                        return;
                    }
                }
            }

            function callMiddleware(middleWare) {
                try{
                    transition.injector().get(middleWare).run();
                }catch(e){
                    console.error('the factory : '+ middleWare+' does not exist');
                }
            };
        });
    });