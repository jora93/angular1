angular
    .module('App')
    .factory('adminMiddleware', function ($state) {
        let authMiddleware = this;

        authMiddleware.run = function () {
            if (!localStorage.isAdmin ) {
                console.error('You are not admin, so you cant browse this');
                $state.go('logout');
            }
        };

        return {
            run: authMiddleware.run
        };

    });