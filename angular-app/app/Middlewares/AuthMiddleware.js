angular
    .module('App')
    .factory('authMiddleware', function ($state) {
        let authMiddleware = this;

        authMiddleware.run = function(){
            if(localStorage.satellizer_token == undefined){
                console.error('You are not logged in, so you cant browse this');
                $state.go('auth');
            }
        };

        return {
            run : authMiddleware.run
        };

    });