<!doctype html>
<!-- <html lang="{{ app()->getLocale() }}" ng-app> -->
<html lang="en" ng-app>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- <script src="{{asset('js/app.bind.js')}}"></script> -->
        <script src="<?php echo asset('js/app.bind.js') ?>"></script>
    </head>
    <body ng-app="routerApp">
    <!-- <body> -->
        <nav class="navbar navbar-inverse" role="navigation">
            <div class="navbar-header">
                <a class="navbar-brand" ui-sref="#">AngularUI Router</a>
            </div>
            <ul class="nav navbar-nav">
                <li><a ui-sref="home">Home</a></li>
                <li><a ui-sref="about">About</a></li>
            </ul>
            <p>Nothing here {{'yet' + '!'}}</p>
        </nav>
        <div class="container">

            <div ui-view></div>

        </div>
    </body>
</html>
